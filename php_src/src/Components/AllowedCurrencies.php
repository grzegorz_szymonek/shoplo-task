<?php

namespace App\Components;

class AllowedCurrencies
{

    const CURRENCY_PLN = 'PLN';
    const CURRENCY_EUR = 'EUR';
    const CURRENCY_USD = 'USD';

    const ALLOWED_CURRENCIES = [
        self::CURRENCY_PLN,
        self::CURRENCY_EUR,
        self::CURRENCY_USD
    ];

    public static function getAvailableCurrencies()
    {
        return array_combine(self::ALLOWED_CURRENCIES, self::ALLOWED_CURRENCIES);
    }

}