<?php

namespace App\Components;

use Faker\Provider\Base as BaseProvider;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

final class FakerPasswordProvider extends BaseProvider
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * PasswordProvider constructor.
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserPasswordEncoderInterface $encoder) {
        $this->encoder = $encoder;
    }

    /**
     * @param $user
     * @param string $plainPassword
     * @return string
     */
    public function encodePassword($user, string $plainPassword): string
    {
        return $this->encoder->encodePassword($user, $plainPassword);
    }
}