<?php

namespace App\Message\Event;

use Ramsey\Uuid\UuidInterface;

class CreatedProductEvent
{
    /**
     * @var UuidInterface
     */
    protected $productId;

    /**
     * ProductCreatedEvent constructor.
     * @param UuidInterface $productId
     */
    public function __construct(UuidInterface $productId)
    {
        $this->productId = $productId;
    }

    /**
     * @return UuidInterface
     */
    public function getProductId(): UuidInterface
    {
        return $this->productId;
    }
}