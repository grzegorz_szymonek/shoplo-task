<?php
namespace App\Message;

class CreateProductCommand
{

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $description;

    /**
     * @var float
     */
    public $price;

    /**
     * @var string
     */
    public $currency;

    /**
     * CreateProductCommand constructor.
     * @param $name
     * @param $description
     * @param $price
     * @param $currency
     */
    public function __construct($name, $description, $price, $currency)
    {
        $this->name = $name;
        $this->description = $description;
        $this->price = $price;
        $this->currency = $currency;
    }


}