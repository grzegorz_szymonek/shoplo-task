<?php

namespace App\Controller;

use App\Repository\Query\ProductQuery;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{

    /**
     * @Route("/", name="homepage")
     * @param ProductQuery $productQuery
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function index(ProductQuery $productQuery, PaginatorInterface $paginator, Request $request): Response
    {

        $pagination = $paginator->paginate(
            $productQuery->getQueryBuilderForPagination(),
            $request->query->get('page', 1),
            10
        );

        return
            $this->render('product/list.html.twig', [
                'pagination' => $pagination
            ]);
    }
}
