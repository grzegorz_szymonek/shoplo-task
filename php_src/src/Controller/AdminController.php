<?php

namespace App\Controller;

use App\DTO\ProductInputDTO;
use App\Message\CreateProductCommand;
use App\Product\Infrastructure\Forms\ProductForm;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @var MessageBusInterface
     */
    private $bus;

    /**
     * AdminController constructor.
     * @param MessageBusInterface $bus
     */
    public function __construct(MessageBusInterface $bus)
    {
        $this->bus = $bus;
    }

    /**
     * @Route("/admin/product/create", name="create_product")
     * @param Request $request
     * @return Response
     */
    public function createProduct(Request $request): Response
    {
        $form = $this->createForm(ProductForm::class, new ProductInputDTO());

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var ProductInputDTO $productInputDTO */
            $productInputDTO = $form->getData();
            $this->bus->dispatch(new CreateProductCommand(
                $productInputDTO->name,
                $productInputDTO->description,
                $productInputDTO->price,
                $productInputDTO->currency
            ));

            return $this->redirectToRoute('homepage');
        }

        return
            $this->render('product/form.html.twig',[
                'form' => $form->createView()
            ]);
    }
}
