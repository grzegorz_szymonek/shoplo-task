<?php

namespace App\Product\Infrastructure\Resources\Notify;

use App\Entity\Product;

interface ProductCreatedNotifyHandlerInterface
{
    /**
     * @param Product $product
     * @return bool
     */
    public function notify(Product $product): bool;
}