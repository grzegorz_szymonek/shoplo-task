<?php

namespace App\Product\Infrastructure\Resources\Notify;

use App\Entity\Product;

class SomeOtherTargetProductCreatedNotifyHandler implements ProductCreatedNotifyHandlerInterface
{

    /**
     * @param Product $product
     * @return bool
     */
    public function notify(Product $product): bool
    {
        dump($product);
        return true;

    }

}