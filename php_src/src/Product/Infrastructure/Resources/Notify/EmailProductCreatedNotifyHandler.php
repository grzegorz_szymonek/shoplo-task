<?php

namespace App\Product\Infrastructure\Resources\Notify;

use App\Entity\Product;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Swift_Mailer;
use Swift_Message;

class EmailProductCreatedNotifyHandler implements ProductCreatedNotifyHandlerInterface
{
    /**
     * @var Swift_Mailer
     */
    private $mailer;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * EmailProductCreatedNotifyHandler constructor.
     * @param Swift_Mailer $mailer
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(Swift_Mailer $mailer, EntityManagerInterface $entityManager)
    {
        $this->mailer = $mailer;
        $this->entityManager = $entityManager;
    }

    /**
     * @param Product $product
     * @return bool
     */
    public function notify(Product $product): bool
    {
        //TODO: make whole this configurable ...
        $message = new Swift_Message();
        $message->setSubject('New Product have just been added')
            ->setFrom('admin@admin.pl')
            ->setBcc($this->prepareEmailAddressList())
            ->setBody($this->prepareMessageString($product), 'text/plain');
        return $this->mailer->send($message);
    }

    private function prepareMessageString(?Product $product): string
    {
        return 'Name: ' . $product->getName() . PHP_EOL .
            'Price: ' . $product->getPrice() . ' ' . $product->getCurrency();
    }

    /**
     * Prepare array of user emails
     * @return array
     */
    private function prepareEmailAddressList(): array
    {
        $users = $this->entityManager->getRepository(User::class)->findAll();
        $emails = [];
        foreach ($users as $user) {
            /** @var User $user */
            $emails[] = $user->getEmail();
        }
        return $emails;
    }
}