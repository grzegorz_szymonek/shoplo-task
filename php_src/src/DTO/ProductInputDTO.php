<?php
namespace App\DTO;

use App\Components\AllowedCurrencies;
use Symfony\Component\Validator\Constraints as Assert;

class ProductInputDTO
{
    /**
     * @var string|null
     * @Assert\NotBlank()
     * @Assert\Length(min=5, max="255")
     */
    public $name;

    /**
     * @var string|null
     * @Assert\NotBlank()
     * @Assert\Length(min=100, max=5000)
     */
    public $description;

    /**
     * @var float|null
     * @Assert\NotBlank()
     * @Assert\GreaterThan(value="1.0")
     */
    public $price;

    /**
     * @var string|null
     * @Assert\Choice(AllowedCurrencies::ALLOWED_CURRENCIES)
     */
    public $currency;


}