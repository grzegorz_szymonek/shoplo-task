<?php

namespace App\Repository;

use App\Entity\Product;

interface ProductRepositoryInterface
{

    /**
     * @param Product $product
     */
    public function create(Product $product);
}
