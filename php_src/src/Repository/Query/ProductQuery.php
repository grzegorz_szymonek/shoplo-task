<?php

namespace App\Repository\Query;

use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\UuidInterface;

class ProductQuery implements ProductQueryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ProductQuery constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @inheritDoc
     */
    public function findOne(UuidInterface $productId): ?Product
    {
        $queryBuilder = $this->entityManager->createQueryBuilder()
            ->select('Product')
            ->from(Product::class, 'Product')
            ->where('Product.id = :productId')
            ->setParameter('productId', $productId);
        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function getQueryBuilderForPagination()
    {
        return $this->entityManager->createQueryBuilder()
            ->select('Product')
            ->from(Product::class, 'Product')
            ->orderBy('Product.createdAt', 'DESC');
    }
}