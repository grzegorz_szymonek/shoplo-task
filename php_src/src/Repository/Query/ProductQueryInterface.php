<?php

namespace App\Repository\Query;

use App\Entity\Product;
use Ramsey\Uuid\UuidInterface;

interface ProductQueryInterface
{
    /**
     * @param UuidInterface $productId
     * @return Product|null
     */
    public function findOne(UuidInterface $productId): ?Product;
}