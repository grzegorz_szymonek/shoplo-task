<?php

namespace App\Handler;

use App\Entity\Product;
use App\Message\CreateProductCommand;
use App\Message\Event\CreatedProductEvent;
use App\Product\Infrastructure\Handler\Persist\ProductPersistStrategyInterface;
use App\Repository\ProductRepositoryInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class CreateProductHandler implements MessageHandlerInterface
{


    /**
     * @var MessageBusInterface
     */
    private $eventBus;
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * CreateProductHandler constructor.
     * @param ProductRepositoryInterface $productRepository
     * @param MessageBusInterface $eventBus
     */
    public function __construct(ProductRepositoryInterface $productRepository, MessageBusInterface $eventBus)
    {
        $this->eventBus = $eventBus;
        $this->productRepository = $productRepository;
    }

    public function __invoke(CreateProductCommand $command)
    {
        $product = new Product();
        $product->setId(Uuid::uuid4());
        $product->setName($command->name);
        $product->setDescription($command->description);
        $product->setPrice($command->price * 100);
        $product->setCurrency($command->currency);
        $product->setCreatedAt(new \DateTime());

        try {
            $this->productRepository->create($product);
        } catch (\Exception $e) {
            // TODO: catch persist error
        }

        $this->eventBus->dispatch(new CreatedProductEvent($product->getId()));
    }
}