<?php

namespace App\Handler;

use App\Message\Event\CreatedProductEvent;
use App\Product\Infrastructure\Resources\Notify\EmailProductCreatedNotifyHandler;
use App\Repository\Query\ProductQueryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ProductCreatedNotifyViaMailHandler implements MessageHandlerInterface
{

    /**
     * @var ProductQueryInterface
     */
    private $productQuery;
    /**
     * @var EmailProductCreatedNotifyHandler
     */
    private $emailProductCreatedNotifyHandler;

    /**
     * EmailProductCreatedNotifyHandler constructor.
     * @param ProductQueryInterface $productQuery
     * @param EmailProductCreatedNotifyHandler $emailProductCreatedNotifyHandler
     */
    public function __construct(ProductQueryInterface $productQuery, EmailProductCreatedNotifyHandler $emailProductCreatedNotifyHandler)
    {
        $this->productQuery = $productQuery;
        $this->emailProductCreatedNotifyHandler = $emailProductCreatedNotifyHandler;
    }

    public function __invoke(CreatedProductEvent $event)
    {
        $this->emailProductCreatedNotifyHandler->notify($this->productQuery->findOne($event->getProductId()));
    }
}