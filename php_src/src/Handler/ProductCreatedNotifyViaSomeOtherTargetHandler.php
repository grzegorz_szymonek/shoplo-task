<?php

namespace App\Handler;

use App\Message\Event\CreatedProductEvent;
use App\Product\Infrastructure\Resources\Notify\SomeOtherTargetProductCreatedNotifyHandler;
use App\Repository\Query\ProductQueryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ProductCreatedNotifyViaSomeOtherTargetHandler implements MessageHandlerInterface
{
    /**
     * @var ProductQueryInterface
     */
    private $productQuery;

    /**
     * @var SomeOtherTargetProductCreatedNotifyHandler
     */
    private $someOtherTargetProductCreatedNotifyHandler;

    /**
     * ProductCreatedNotifyViaSomeOtherTargetHandler constructor.
     * @param ProductQueryInterface $productQuery
     * @param SomeOtherTargetProductCreatedNotifyHandler $someOtherTargetProductCreatedNotifyHandler
     */
    public function __construct(ProductQueryInterface $productQuery, SomeOtherTargetProductCreatedNotifyHandler $someOtherTargetProductCreatedNotifyHandler)
    {
        $this->productQuery = $productQuery;
        $this->someOtherTargetProductCreatedNotifyHandler = $someOtherTargetProductCreatedNotifyHandler;
    }

    public function __invoke(CreatedProductEvent $event)
    {
        $this->someOtherTargetProductCreatedNotifyHandler->notify($this->productQuery->findOne($event->getProductId()));
    }

}