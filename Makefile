reload-database:
	-docker-compose exec php-fpm bin/console doctrine:database:drop --force
	-docker-compose exec php-fpm bin/console doctrine:database:create
	-docker-compose exec php-fpm bin/console doctrine:schema:update --force
	-docker-compose exec php-fpm bin/console hautelook:fixtures:load --purge-with-truncate -n