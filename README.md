## install:
    docker-compose build

## Start:
    docker-compose up


## db init:
### if can use make:
    make
### else:
	docker-compose exec php-fpm bin/console doctrine:database:drop --force
	docker-compose exec php-fpm bin/console doctrine:database:create
	docker-compose exec php-fpm bin/console doctrine:schema:update --force
	docker-compose exec php-fpm bin/console hautelook:fixtures:load --purge-with-truncate -n


## Goto: 
http://localhost:8080

### initial admin login and pass:
    login: admin@admin.com
    pass: admin123